#ifndef _DATATYPES_H
#define _DATATYPES_H

namespace os {
	namespace types 
	{
		typedef char int8_t;
		typedef unsigned char uint8_t;
		typedef short int16_t;
		typedef unsigned short uint16_t;
		typedef int int32_t;
		typedef unsigned uint32_t;
		typedef long long int64_t;
		typedef unsigned long long uint64_t;

		typedef const char* char_string;
		typedef int32_t ssize_t;
		typedef uint32_t size_t;
	}
}

#endif
