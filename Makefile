#dependencies : g++ binutils libc6-dev-i386 VirtualBox grub-legacy xorriso


objects = boot.o interrupts_asm.o io.o port.o gdt.o interrupts.o kernel.o

%.o: %.cpp
	g++ -std=c++14 -nostdlib \
	-fno-builtin \
	-fno-rtti \
	-fno-exceptions \
	-fno-leading-underscore \
	-Wno-write-strings \
	-ffreestanding \
	-fno-use-cxa-atexit -m32 -c -o $@ $<

%.o: %.s
	as --32 -o $@ $<

kernel.bin: linker.ld $(objects)
	ld -melf_i386 -T $< -o $@ $(objects)

kernel.iso: kernel.bin
	mkdir iso
	mkdir iso/boot
	mkdir iso/boot/grub
	cp kernel.bin iso/boot/kernel.bin
	echo 'set timeout=0\nset default=0\n' >> iso/boot/grub/grub.cfg
	echo 'menuentry "Custom Operating System" {\n  multiboot /boot/kernel.bin\n  boot\n}' >> iso/boot/grub/grub.cfg
	grub-mkrescue --output=kernel.iso iso
	./verify_multiboot.sh
	rm -rf iso
	rm $(objects) kernel.bin



install: kernel.bin
	sudo cp $< /boot/mykernel.bin
