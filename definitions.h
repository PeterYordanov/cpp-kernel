#ifndef _DEFINITIONS_H
#define _DEFINITIONS_H

#define UNUSED(x) (void)x
#define NUMBER_TO_MEGABYTES(x) x * 1024 * 1024
#define PACKED __attribute__((packed))
#define ASM(x) __asm__(x)
#define ASM_VOLATILE(x) __asm__ __volatile__(x)

#if __cpp_constexpr >= 201304
	#define CONSTEXPR constexpr
#else
	#define CONSTEXPR const
#endif

#endif //_DEFINITIONS_H
