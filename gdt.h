#ifndef _GDT_H
#define _GDT_H

#include "datatypes.h"

using namespace os::types;

namespace os
{

	class SegmentDescriptor {
	public:
		SegmentDescriptor(uint32_t base, uint32_t limit, uint8_t access);
		uint32_t base() const;
		uint32_t limit() const;

	private:
		uint16_t limit_low;
		uint16_t base_low;
		uint8_t base_high;
		uint8_t access;
		uint8_t limit_high;
		uint8_t base_vhigh;
	} __attribute__((packed));

    

	class GlobalDescriptorTable {
	public:
		GlobalDescriptorTable();
		~GlobalDescriptorTable();

		uint16_t offset_code_segment_selector() const;
		uint16_t offset_data_segment_selector() const;

	private:
		SegmentDescriptor null_segment_selector;
		SegmentDescriptor unused_segment_selector;
		SegmentDescriptor code_segment_selector;
		SegmentDescriptor data_segment_selector;
	};

}
    
#endif
