#ifndef _IO_H
#define _IO_H

#include "datatypes.h"

namespace os {
	
	namespace io
	{
		void printf(os::types::char_string);
		void printfhex(os::types::uint8_t);
		void printfhex16(os::types::uint16_t);
		void printfhex32(os::types::uint32_t);
	}

};

#endif
