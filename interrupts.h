#ifndef _HW_COMM_INTERRUPTMANAGER_H
#define _HW_COMM_INTERRUPTMANAGER_H

#include "gdt.h"
#include "datatypes.h"
#include "port.h"

using namespace os::types;
using namespace os::hw_comm;

namespace os
{
	namespace hw_comm
	{
		struct GateDescriptor {
			uint16_t handler_addr_low_bits;
			uint16_t gdt_code_segment_selector;
			uint8_t  reserved;
			uint8_t  access;
			uint16_t handler_addr_high_bits;
		} __attribute__((packed));

		struct InterruptDescriptorTablePtr {
			uint16_t size;
			uint32_t base;
		} __attribute__((packed));


		class InterruptManager;
		class InterruptHandler {
		protected:
			uint8_t 	  interrupt_number;
			InterruptManager* interrupt_manager;
			
			InterruptHandler(InterruptManager*, uint8_t interrupt_number);
			~InterruptHandler();
		public:
			virtual uint32_t handle_interrupt(uint32_t esp);
		};


		class InterruptManager {
			friend class InterruptHandler;

		public:
			InterruptManager(uint16_t hardware_interrupt_offset, GlobalDescriptorTable* gdt);
			~InterruptManager();
			
			uint16_t hardware_interrupt_offset() const;
			
			void activate();
			void deactivate();
	
		protected:
			static InterruptManager* active_interrupt_manager;
			InterruptHandler* 	 handlers[256];

			static GateDescriptor interruptDescriptorTable[256];
			uint16_t hw_interrupt_offset;

			static void set_idt_entry(uint8_t interrupt,
						  uint16_t code_segment_selector_offset, 
						  void (*handler)(),
						  uint8_t privilege_lvl, 
						  uint8_t type);


			static void interrupt_ignore();

			static void handle_interrupt_request0x00();
			static void handle_interrupt_request0x01();
			static void handle_interrupt_request0x02();
			static void handle_interrupt_request0x03();
			static void handle_interrupt_request0x04();
			static void handle_interrupt_request0x05();
			static void handle_interrupt_request0x06();
			static void handle_interrupt_request0x07();
			static void handle_interrupt_request0x08();
			static void handle_interrupt_request0x09();
			static void handle_interrupt_request0x0A();
			static void handle_interrupt_request0x0B();
			static void handle_interrupt_request0x0C();
			static void handle_interrupt_request0x0D();
			static void handle_interrupt_request0x0E();
			static void handle_interrupt_request0x0F();
			static void handle_interrupt_request0x31();
			static void handle_interrupt_request0x80();

			static void handle_exception0x00();
			static void handle_exception0x01();
			static void handle_exception0x02();
			static void handle_exception0x03();
			static void handle_exception0x04();
			static void handle_exception0x05();
			static void handle_exception0x06();
			static void handle_exception0x07();
			static void handle_exception0x08();
			static void handle_exception0x09();
			static void handle_exception0x0A();
			static void handle_exception0x0B();
			static void handle_exception0x0C();
			static void handle_exception0x0D();
			static void handle_exception0x0E();
			static void handle_exception0x0F();
			static void handle_exception0x10();
			static void handle_exception0x11();
			static void handle_exception0x12();
			static void handle_exception0x13();
								
			static uint32_t handle_interrupt(uint8_t interrupt, uint32_t esp);
			uint32_t do_handle_interrupt(uint8_t, uint32_t);

			Port8BitSlow pic_master_command_port;
			Port8BitSlow pic_master_data_port;
			Port8BitSlow pic_slave_command_port;
			Port8BitSlow pic_slave_data_port;
		};	
		
	} //namespace hw_comm
} //namespace os

#endif
