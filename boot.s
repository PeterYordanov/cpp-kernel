.set MAGIC, 0x1badb002
.set FLAGS, (1<<0 | 1<<1)
.set CHECKSUM, -(MAGIC + FLAGS)

.section .multiboot
	.align 4
	.long MAGIC
	.long FLAGS
	.long CHECKSUM

.section .bss
.align 16
stack_bottom:
.skip 16384 #16 kilobytes free stack space
stack_top:

.section .text
.extern kernel_main
.extern call_constructors
.global _start


_start:
	mov $stack_top, %esp
	call call_constructors
	push %eax
	push %ebx
	call kernel_main


_infinite_loop:
	cli
	hlt
	jmp _infinite_loop

