#include "interrupts.h"
#include "io.h"

using namespace os;
using namespace os::hw_comm;

GateDescriptor InterruptManager::interruptDescriptorTable[256];
InterruptManager* InterruptManager::active_interrupt_manager = 0;

void InterruptManager::set_idt_entry(uint8_t interrupt,
                                     uint16_t code_segment, 
                                     void (*handler)(), 
                                     uint8_t privilege_lvl, 
                                     uint8_t type)
{
	interruptDescriptorTable[interrupt].handler_addr_low_bits = ((uint32_t) handler) & 0xFFFF;
	interruptDescriptorTable[interrupt].handler_addr_high_bits = (((uint32_t) handler) >> 16) & 0xFFFF;
	interruptDescriptorTable[interrupt].gdt_code_segment_selector = code_segment;

	constexpr uint8_t IDT_DESC_PRESENT = 0x80;
	interruptDescriptorTable[interrupt].access = IDT_DESC_PRESENT | ((privilege_lvl & 0x03) << 0x05) | type;
	interruptDescriptorTable[interrupt].reserved = 0;
}


InterruptManager::InterruptManager(uint16_t hardware_interrupt_offset, GlobalDescriptorTable* gdt)
	: pic_master_command_port(0x20),
	  pic_master_data_port(0x21),
	  pic_slave_command_port(0xA0),
	  pic_slave_data_port(0xA1)
{
	this->hw_interrupt_offset = hardware_interrupt_offset;
	uint32_t code_segment = gdt->offset_code_segment_selector();

	constexpr uint8_t IDT_INTERRUPT_GATE = 0xE;
	for(uint8_t i = 255; i > 0; --i) {
		set_idt_entry(i, code_segment, &interrupt_ignore, 0, IDT_INTERRUPT_GATE);
		handlers[i] = 0;
	}

	set_idt_entry(0, code_segment, &interrupt_ignore, 0, IDT_INTERRUPT_GATE);
	handlers[0] = 0;

	set_idt_entry(0x00, code_segment, &handle_exception0x00, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x01, code_segment, &handle_exception0x01, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x02, code_segment, &handle_exception0x02, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x03, code_segment, &handle_exception0x03, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x04, code_segment, &handle_exception0x04, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x05, code_segment, &handle_exception0x05, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x06, code_segment, &handle_exception0x06, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x07, code_segment, &handle_exception0x07, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x08, code_segment, &handle_exception0x08, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x09, code_segment, &handle_exception0x09, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x0A, code_segment, &handle_exception0x0A, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x0B, code_segment, &handle_exception0x0B, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x0C, code_segment, &handle_exception0x0C, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x0D, code_segment, &handle_exception0x0D, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x0E, code_segment, &handle_exception0x0E, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x0F, code_segment, &handle_exception0x0F, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x10, code_segment, &handle_exception0x10, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x11, code_segment, &handle_exception0x11, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x12, code_segment, &handle_exception0x12, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(0x13, code_segment, &handle_exception0x13, 0, IDT_INTERRUPT_GATE);

	set_idt_entry(hardware_interrupt_offset + 0x00, code_segment, &handle_interrupt_request0x00, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x01, code_segment, &handle_interrupt_request0x01, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x02, code_segment, &handle_interrupt_request0x02, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x03, code_segment, &handle_interrupt_request0x03, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x04, code_segment, &handle_interrupt_request0x04, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x05, code_segment, &handle_interrupt_request0x05, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x06, code_segment, &handle_interrupt_request0x06, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x07, code_segment, &handle_interrupt_request0x07, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x08, code_segment, &handle_interrupt_request0x08, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x09, code_segment, &handle_interrupt_request0x09, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x0A, code_segment, &handle_interrupt_request0x0A, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x0B, code_segment, &handle_interrupt_request0x0B, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x0C, code_segment, &handle_interrupt_request0x0C, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x0D, code_segment, &handle_interrupt_request0x0D, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x0E, code_segment, &handle_interrupt_request0x0E, 0, IDT_INTERRUPT_GATE);
	set_idt_entry(hardware_interrupt_offset + 0x0F, code_segment, &handle_interrupt_request0x0F, 0, IDT_INTERRUPT_GATE);

	set_idt_entry(				0x80, code_segment, &handle_interrupt_request0x80, 0, IDT_INTERRUPT_GATE);

	pic_master_command_port.write(0x11);
	pic_slave_command_port.write(0x11);

	pic_master_data_port.write(hardware_interrupt_offset);
	pic_slave_data_port.write(hardware_interrupt_offset + 8);

	pic_master_data_port.write(0x04);
	pic_slave_data_port.write(0x02);

	pic_master_data_port.write(0x01);
	pic_slave_data_port.write(0x01);

	pic_master_data_port.write(0x00);
	pic_slave_data_port.write(0x00);

	InterruptDescriptorTablePtr idt_ptr;
	idt_ptr.size  = 256 * sizeof(GateDescriptor) - 1;
	idt_ptr.base  = (uint32_t)interruptDescriptorTable;
	__asm__ __volatile__("lidt %0" : : "m" (idt_ptr));
}

InterruptManager::~InterruptManager()
{
	deactivate();
}

uint16_t InterruptManager::hardware_interrupt_offset() const
{
	return hw_interrupt_offset;
}

void InterruptManager::activate()
{
	if(active_interrupt_manager != 0)
		active_interrupt_manager->deactivate();

	active_interrupt_manager = this;
	__asm__("sti");
}

void InterruptManager::deactivate()
{
	if(active_interrupt_manager == this) {
		active_interrupt_manager = 0;
		__asm__("cli");
	}
}

uint32_t InterruptManager::handle_interrupt(uint8_t interrupt, uint32_t esp)
{
	if(active_interrupt_manager != 0)
		return active_interrupt_manager->do_handle_interrupt(interrupt, esp);
	return esp;
}


uint32_t InterruptManager::do_handle_interrupt(uint8_t interrupt, uint32_t stack_pointer)
{
	//...
	return stack_pointer;
}


InterruptHandler::InterruptHandler(InterruptManager* interrupt_manager, uint8_t interrupt_number)
{
	this->interrupt_number = interrupt_number;
	this->interrupt_manager = interrupt_manager;
	interrupt_manager->handlers[interrupt_number] = this;
}

InterruptHandler::~InterruptHandler()
{
	if(interrupt_manager->handlers[interrupt_number] == this) {
		interrupt_manager->handlers[interrupt_number] = 0;
	}
}

uint32_t InterruptHandler::handle_interrupt(uint32_t esp)
{
	return esp;
}
