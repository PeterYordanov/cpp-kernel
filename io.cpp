#include "io.h"

void os::io::printf(os::types::char_string str) 
{
	using namespace os::types;

	uint16_t* v_mem = (uint16_t*)0xb8000;
	static uint8_t x = 0, y = 0;

	auto insert_new_line = []() {
		x = 0;
		y++;
	};

	for(int i = 0; str[i] != '\0'; i++) {

		if(str[i] == '\n') {
			insert_new_line();
		} else {
			v_mem[80 * y + x] = (v_mem[80 * y + x] & 0xff00) | str[i];
			x++;		
		}

		if(x >= 80) {
			insert_new_line();
		}
		
		if(y >= 25) {
			for(y = 0; y < 25; y++) {
				for(x = 0; x < 80; x++) {
					v_mem[80 * y + x] = (v_mem[80 * y + x] & 0xff00) | ' ';	
				}
			}
			
			x = 0;
			y = 0;
		}
	}	
}


void os::io::printfhex(os::types::uint8_t key)
{
	char* foo = "00";
	char* hex = "0123456789ABCDEF";
	foo[0] = hex[(key >> 4) & 0xF];
	foo[1] = hex[key & 0xF];
	printf(foo);
}
void os::io::printfhex16(os::types::uint16_t key)
{
	printfhex((key >> 8) & 0xFF);
	printfhex( key & 0xFF);
}

void os::io::printfhex32(os::types::uint32_t key)
{
	printfhex((key >> 24) & 0xFF);
	printfhex((key >> 16) & 0xFF);
	printfhex((key >> 8) & 0xFF);
	printfhex( key & 0xFF);
}
