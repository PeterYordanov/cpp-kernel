#include "datatypes.h"
#include "gdt.h"
#include "interrupts.h"
#include "io.h"

typedef void (*constructor)();
extern "C" constructor start_constructors;
extern "C" constructor end_constructors;
extern "C" void call_constructors()
{
	for(constructor* i = &start_constructors; i != &end_constructors; i++) {
		(*i)();
	}
}


extern "C" void kernel_main(const void* multiboot_structure, os::types::uint32_t /*multiboot_magic*/)
{
	os::io::printf("Test\n");

	os::GlobalDescriptorTable gdt;

	os::hw_comm::InterruptManager interrupts(0x20, &gdt); 
	interrupts.activate();
	
	while(1);
}
